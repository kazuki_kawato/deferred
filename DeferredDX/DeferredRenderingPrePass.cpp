#include "stdafx.h"
#include "DXLib.h"
#include "DeferredRenderingPrePass.h"


RenderingPass g_BasePass;
ID3D11Texture2D *pNormalBuffer = nullptr;
ID3D11Texture2D* pBasePassDepthBuffer = nullptr;

ID3D11VertexShader *g_pVertexShader_Pass0 = nullptr;
ID3D11PixelShader *g_pPixelShader_Pass0 = nullptr;

HRESULT DXLib_CreateDeferredBasePassResource(ID3D11Device *pDevice)
{
	HRESULT hr = S_OK;
	// Gバッファ(法線)
	D3D11_TEXTURE2D_DESC normalBufferDesc;
	ZeroMemory(&normalBufferDesc, sizeof(normalBufferDesc));
	normalBufferDesc.Width = 1024;
	normalBufferDesc.Height = 768;
	normalBufferDesc.MipLevels = 1;
	normalBufferDesc.ArraySize = 1;
	normalBufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	normalBufferDesc.SampleDesc.Count = 1;
	normalBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	normalBufferDesc.BindFlags = 32;

	if (FAILED(hr = pDevice->CreateTexture2D(&normalBufferDesc, nullptr, &pNormalBuffer)))
	{
		return hr;
	}

	// depth stencil viewの設定
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = normalBufferDesc.Width;
	descDepth.Height = normalBufferDesc.Height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	if (FAILED(hr = pDevice->CreateTexture2D(&descDepth, nullptr, &pBasePassDepthBuffer))) {
		return hr;
	}

	if (FAILED(g_BasePass.UpdateViews(pDevice, 1, &pNormalBuffer, pBasePassDepthBuffer)))
	{
		return hr;
	}
	ID3DBlob *pErrorMsgBuffer		 = nullptr;
	ID3DBlob *pVertexShaderBuffer	 = nullptr;
	ID3DBlob *pPixelShaderBuffer	 = nullptr;

	//if (FAILED(hr = D3DX11CompileFromFile(L"VertexShader_Pass0.hlsl", NULL, NULL, "main", "vs_4_0_level_9_1", 0, 0, NULL,
	//	&pVertexShaderBuffer, &pErrorMsgBuffer, NULL)))
	//{
	//	return hr;
	//}

	hr = DXLibLoadCompiledShader(L"VertexShader_Pass0.cso", &pVertexShaderBuffer);
	if (FAILED(hr)) {
		return hr;
	}

	//if (FAILED(D3DX11CompileFromFile(L"PixelShader_Pass0.hlsl", NULL, NULL, "main", "ps_4_0_level_9_1", 0, 0, NULL,
	//	&pPixelShaderBuffer, &pErrorMsgBuffer, NULL)))
	//{
	//	return hr;
	//}

	hr = DXLibLoadCompiledShader(L"PixelShader_Pass0.cso", &pPixelShaderBuffer);
	if (FAILED(hr)) {
		return hr;
	}

	if (FAILED(hr = pDevice->CreateVertexShader(pVertexShaderBuffer->GetBufferPointer(),
		pVertexShaderBuffer->GetBufferSize(), nullptr, &g_pVertexShader_Pass0)))
	{
		return hr;
	}

	if (FAILED(hr = pDevice->CreatePixelShader(pPixelShaderBuffer->GetBufferPointer(),
		pPixelShaderBuffer->GetBufferSize(), nullptr, &g_pPixelShader_Pass0)))
	{
		return hr;
	}

	// シェーダ用のバッファを開放
	SAFE_RELEASE(pVertexShaderBuffer);
	SAFE_RELEASE(pPixelShaderBuffer);


}

void DXLib_RenderDeferredBasePass(ID3D11DeviceContext *pImmediateContext)
{

}

void DXLib_ReleaseDeferredBasePassResource()
{
	g_BasePass.ReleaseViews();

	SAFE_RELEASE(pNormalBuffer);
	SAFE_RELEASE(pBasePassDepthBuffer);
	SAFE_RELEASE(g_pVertexShader_Pass0);
	SAFE_RELEASE(g_pPixelShader_Pass0);
}

RenderingPass::RenderingPass()
{
	ZeroMemory(&passSettings, sizeof(passSettings));
	numRenderTarget = 0;
	ppRenderTargetViews = nullptr;
	pDepthStencilView = nullptr;
}


RenderingPass::~RenderingPass()
{
	ZeroMemory(&passSettings, sizeof(passSettings));
	ReleaseViews();
}

// バッファからViewを作成する
HRESULT RenderingPass::UpdateViews(ID3D11Device *pDevice, UINT numRT, ID3D11Texture2D **ppRenderTargetBuffers, ID3D11Texture2D *pDepthBuffers)
{
	HRESULT hr = S_OK;
	ReleaseViews();

	numRenderTarget = numRT;

	//passSettings.ppTargetBuffers = new ID3D11Texture2D *[numRenderTarget];
	ppRenderTargetViews = new ID3D11RenderTargetView *[numRenderTarget];

	for (UINT i = 0; i < numRenderTarget; i++) {
		ID3D11RenderTargetView *pRTV = nullptr;
		pDevice->CreateRenderTargetView(ppRenderTargetBuffers[i], nullptr, &pRTV);
		ppRenderTargetViews[i] = pRTV;
	}

	D3D11_TEXTURE2D_DESC depthDesc;
	pDepthBuffers->GetDesc(&depthDesc);

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = depthDesc.Format;
	descDSV.Flags = 0;
	if (depthDesc.SampleDesc.Count > 1)
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	else
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	ID3D11DepthStencilView *pDSV = nullptr;

	// depth stencil view を作る
	if (FAILED(hr = pDevice->CreateDepthStencilView(pDepthBuffers, &descDSV, &pDSV)))
	{
		return hr;
	}
	pDepthStencilView = pDSV;

	return hr;
}

void RenderingPass::SetD3D11Shaders(ID3D11VertexShader *pVertexShader, ID3D11PixelShader *pPixelShader)
{
	passSettings.pVertexShader = pVertexShader;
	passSettings.pPixelShader = pPixelShader;
}

void RenderingPass::RenderPass(ID3D11DeviceContext *pImmediateContext)
{
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	pImmediateContext->OMSetRenderTargets(numRenderTarget, ppRenderTargetViews, pDepthStencilView);
	for (UINT i = 0; i < numRenderTarget; i++)
	{
		pImmediateContext->ClearRenderTargetView(ppRenderTargetViews[i], ClearColor);
	}
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);

	pImmediateContext->VSSetShader(passSettings.pVertexShader, nullptr, 0);
	pImmediateContext->PSSetShader(passSettings.pPixelShader, nullptr, 0);

}

// 作成したViewを開放する
void RenderingPass::ReleaseViews()
{
	for (UINT i = 0; i < numRenderTarget; i++) {
		SAFE_RELEASE(ppRenderTargetViews[i]);
	}
	
	SAFE_RELEASE(pDepthStencilView);
	SAFE_DELETE_ARRAY(ppRenderTargetViews);
	numRenderTarget = 0;
}


