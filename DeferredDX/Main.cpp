#include "stdafx.h"
#include "DXLib.h"
#include "DeferredRenderingPrePass.h"

#define SAFE_RELEASE(a) {if(a){ (a)->Release(); (a) = nullptr; }	}

ID3D11VertexShader	*g_pVertexShader	= nullptr;
ID3D11PixelShader	*g_pPixelShader		= nullptr;

ID3D11InputLayout		*g_pLayout11		= nullptr;
ID3D11SamplerState		*g_pSamLinear		= nullptr;

D3DXMATRIX mModel;
D3DXMATRIX mView;
D3DXMATRIX mProj;

struct CB_VS_PER_OBJECT
{
	D3DXMATRIX  m_mWorldViewProjection;
	D3DXMATRIX  m_mWorld;
	D3DXVECTOR4 m_MaterialAmbientColor;
	D3DXVECTOR4 m_MaterialDiffuseColor;
};

struct Vertex
{
	float position[3];
	float normal[3];
	float albedo[4];
	float specular[4];
	//float tex[2];
};

// 頂点データ
Vertex vertex[3] = {
	{ { -0.5f, 0.5f, 0.5f },  {0.0f, 0.0f, 1.0f}, {0.5f, 0.5f, 0.5f, 0.0f}, { 0.5f, 0.5f, 0.5f, 0.0f } },
	{ { -0.5f, -0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f },{ 0.5f, 0.5f, 0.5f, 0.0f }, { 0.5f, 0.5f, 0.5f, 0.0f } },
	{ { 0.5f, -0.5f, 0.5f }, { 0.0f, 0.0f, 1.0f },{ 0.5f, 0.5f, 0.5f, 0.0f }, { 0.5f, 0.5f, 0.5f, 0.0f } },
};
ID3D11Buffer* g_pVBuffer = nullptr;

//ID3D11

// 遅延レンダリング用のRender Target Viewを作成する
HRESULT CreateAdditionalView(ID3D11Device *pDevice, ID3D11DeviceContext *pImmediateContext) {
	
	DXLib_CreateDeferredBasePassResource(pDevice);

	//HRESULT hr = S_OK;

	//// Gバッファ(法線)
	//ID3D11Texture2D *pNormalBuffer = NULL;
	//ID3D11RenderTargetView *pNormalRTV = NULL;
	//ID3D11DepthStencilView *pNormalDSV = NULL;
	//D3D11_TEXTURE2D_DESC normalBufferDesc;
	//ZeroMemory(&normalBufferDesc, sizeof(normalBufferDesc));
	//normalBufferDesc.Width = 1024;
	//normalBufferDesc.Height = 768;
	//normalBufferDesc.MipLevels = 1;
	//normalBufferDesc.ArraySize = 1;
	//normalBufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//normalBufferDesc.SampleDesc.Count = 1;
	//normalBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//normalBufferDesc.BindFlags = 32;

	//hr = pDevice->CreateTexture2D(&normalBufferDesc, NULL, &pNormalBuffer);
	//
	//pDevice->CreateRenderTargetView(pNormalBuffer, NULL, &pNormalRTV);

	//// depth stencil viewの設定
	//ID3D11Texture2D* pDepthStencil = NULL;
	//D3D11_TEXTURE2D_DESC descDepth;
	//descDepth.Width = normalBufferDesc.Width;
	//descDepth.Height = normalBufferDesc.Height;
	//descDepth.MipLevels = 1;
	//descDepth.ArraySize = 1;
	//descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	//descDepth.SampleDesc.Count = 1;
	//descDepth.SampleDesc.Quality = 0;
	//descDepth.Usage = D3D11_USAGE_DEFAULT;
	//descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	//descDepth.CPUAccessFlags = 0;
	//descDepth.MiscFlags = 0;
	//hr = pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
	//if (FAILED(hr))
	//	return hr;

	//D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	//descDSV.Format = descDepth.Format;
	//descDSV.Flags = 0;
	//if (descDepth.SampleDesc.Count > 1)
	//	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	//else
	//	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	//descDSV.Texture2D.MipSlice = 0;

	//// depth stencil view を作る
	//hr = pDevice->CreateDepthStencilView(pDepthStencil, &descDSV, &pNormalDSV);

	return S_OK;
}

HRESULT OnD3D11SwapChainUpdated(DXLibDeviceResource *pResource)
{
	HRESULT hr = S_OK;

	ID3D11Device *pDevice = pResource->pD3D11Device;
	ID3D11DeviceContext *pImmediateContext = pResource->pImmediateContext;

	CreateAdditionalView(pDevice, pImmediateContext);

	return hr;
}

HRESULT OnD3D11CreateDevice(DXLibDeviceResource *pResource)
{
	HRESULT hr = S_OK;

	ID3D11Device *pDevice = pResource->pD3D11Device;
	ID3D11DeviceContext *pImmediateContext = pResource->pImmediateContext;

	ID3DBlob* pVertexShaderBuffer = nullptr;
	ID3DBlob* pPixelShaderBuffer = nullptr;
	
	hr = DXLibLoadCompiledShader(L"VertexShader.cso", &pVertexShaderBuffer);
	if (FAILED(hr)) {
		return hr;
	}
	
	hr = DXLibLoadCompiledShader(L"PixelShader.cso", &pPixelShaderBuffer);
	if (FAILED(hr)) {
		return hr;
	}
	
	hr = pDevice->CreateVertexShader(pVertexShaderBuffer->GetBufferPointer(),
		pVertexShaderBuffer->GetBufferSize(), nullptr, &g_pVertexShader);

	hr = pDevice->CreatePixelShader(pPixelShaderBuffer->GetBufferPointer(),
		pPixelShaderBuffer->GetBufferSize(), nullptr, &g_pPixelShader);

	const D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 40, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = pDevice->CreateInputLayout(layout, sizeof(layout)/sizeof(layout[0]), pVertexShaderBuffer->GetBufferPointer(),
		pVertexShaderBuffer->GetBufferSize(), &g_pLayout11);
	

	// シェーダ用のバッファを開放
	SAFE_RELEASE(pVertexShaderBuffer);
	SAFE_RELEASE(pPixelShaderBuffer);

	// Sampler Stateを作成
	D3D11_SAMPLER_DESC samDesc;
	ZeroMemory(&samDesc, sizeof(samDesc));
	samDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samDesc.AddressU = samDesc.AddressV = samDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samDesc.MaxAnisotropy = 1;
	samDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = pDevice->CreateSamplerState(&samDesc, &g_pSamLinear);
	
	// VertexBufferの作成
	D3D11_BUFFER_DESC hBufferDesc;
	ZeroMemory(&hBufferDesc, sizeof(hBufferDesc));
	hBufferDesc.ByteWidth = sizeof(Vertex) * 3;
	hBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	hBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	hBufferDesc.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA hSubResourceData;
	ZeroMemory(&hSubResourceData, sizeof(hSubResourceData));
	hSubResourceData.pSysMem = vertex;
	hSubResourceData.SysMemPitch = 0;
	hSubResourceData.SysMemSlicePitch = 0;

	hr = pDevice->CreateBuffer(&hBufferDesc, &hSubResourceData, &g_pVBuffer);

	// シェーダーとサンプラーをセット
	pImmediateContext->VSSetShader(g_pVertexShader, nullptr, 0);
	pImmediateContext->PSSetShader(g_pPixelShader, nullptr, 0);
	pImmediateContext->PSSetSamplers(0, 1, &g_pSamLinear);
	
	return hr;
}

void OnD3D11PreRenderring(DXLibDeviceResource *pResource)
{

}

void OnD3D11Render(DXLibDeviceResource *pResource)
{
	HRESULT hr = S_OK;
	ID3D11DeviceContext *pImmediateContext = pResource->pImmediateContext;

	DXLib_RenderDeferredBasePass(pImmediateContext);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	// Set render resources
	pImmediateContext->IASetVertexBuffers(0, 1, &g_pVBuffer, &stride, &offset);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImmediateContext->IASetInputLayout(g_pLayout11);

	pImmediateContext->Draw(3, 0);

	float ClearColor[4] = { 0.176f, 0.196f, 0.667f, 0.0f };
	
	//D3DXMATRIX mWorld = *g_Camera.GetWorldMatrix();
	//D3DXMATRIX mView = *g_Camera.GetViewMatrix();
	//D3DXMATRIX mProj = *g_Camera.GetProjMatrix();
	D3DXMATRIX mWorldViewProjection = mWorld * mView * mProj;

	ID3D11RenderTargetView* pRTV = pResource->pRenderTargetView;
	ID3D11DepthStencilView* pDSV = pResource->pDepthStencilView;
	pImmediateContext->ClearRenderTargetView(pRTV, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);

	D3D11_MAPPED_SUBRESOURCE MappedResource;
	//hr = pImmediateContext->Map(g_pcbVSPerObject11, 0, D3D11_MAP_WRITE_DISCARD, 0, &MappedResource);

	//CB_VS_PER_OBJECT* pVSPerObject = (CB_VS_PER_OBJECT*)MappedResource.pData;
	//D3DXMatrixTranspose(&pVSPerObject->m_mWorldViewProjection, &mWorldViewProjection);
	//D3DXMatrixTranspose(&pVSPerObject->m_mWorld, &mWorld);
	//pVSPerObject->m_MaterialAmbientColor = D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f);
	//pVSPerObject->m_MaterialDiffuseColor = D3DXVECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
	//pImmediateContext->Unmap(g_pcbVSPerObject11, 0);
	//pImmediateContext->VSSetConstantBuffers(0, 1, &g_pcbVSPerObject11);


	// Set render resources
	pImmediateContext->IASetVertexBuffers(0, 1, &g_pVBuffer, &stride, &offset);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pImmediateContext->IASetInputLayout(g_pLayout11);

	pImmediateContext->Draw(3, 0);

}

void OnD3D11Release(DXLibDeviceResource *pResource)
{
	DXLib_ReleaseDeferredBasePassResource();

	SAFE_RELEASE(g_pLayout11);
	SAFE_RELEASE(g_pVertexShader);
	SAFE_RELEASE(g_pPixelShader);
	SAFE_RELEASE(g_pSamLinear);
	SAFE_RELEASE(g_pVBuffer);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	D3DXMatrixIdentity(&mWorld);
	D3DXMatrixIdentity(&mView);
	D3DXMatrixOrthoLH(&mProj, 1.0f, 1.0f, 0.01f, 1000.0f);

	DXLibSetD3D11CreateDeviceFunc(OnD3D11CreateDevice);
	DXLibSetD3D11SwapChainUpdatedFunc(OnD3D11SwapChainUpdated);
	DXLibSetD3D11PreRenderFunc(OnD3D11PreRenderring);
	DXLibSetD3D11RenderFunc(OnD3D11Render);
	DXLibSetD3D11ReleaseFunc(OnD3D11Release);

	DXLibCreateWindow(L"れんだりんぐ", L"Rendering");
	DXLibCreateDevice(1024, 768);
	DXLibMainLoop();

	DXLibRelease();

	return 0;
}