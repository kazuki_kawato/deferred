struct VS_INPUT
{
	float4 Position : POSITION;
	float4 Normal : NORMAL;
	float4 Albedo : COLOR0;
	float4 Specular : COLOR1;
};

struct VS_OUTPUT
{
	float4 Position : SV_POSITION;
	float4 Normal : NORMAL0;
	float4 Albedo : COLOR0;
	float4 Specular : COLOR1;
};

VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Position = input.Position;
	output.Normal.xyz = input.Normal.xyz;
	output.Normal.w = 0;
	output.Albedo = input.Albedo;
	output.Specular = output.Albedo;
	return output;

}