#include "DXLib.h"

//ゲッター　セッター定義用のマクロ
#define GET_ACCESSOR(type, var, funcname) type DXLibGet##funcname(){ return var; }
#define SET_ACCESSOR(type, var, funcname) void DXLibSet##funcname(type t){ var = t; }
#define GET_SET_ACCESSOR(type, var, funcname) GET_ACCESSOR(type, var, funcname); SET_ACCESSOR(type, var, funcname);

#define SAFE_RELEASE(a) {if(a){ (a)->Release(); (a) = NULL; }	}

const int defWindowWidth = 1024;
const int defWindowHeight = 768;

DXLibDeviceSettings devSettings;
DXLibDeviceResource devResource;


struct DXLibCallbackFunc {
	DXLibD3D11RenderFunc pRenderFunc;
	DXLibD3D11PreRenderFunc pPreRenderFunc;
	DXLibD3D11CreateDeviceFunc pCreateDeviceFunc;
	DXLibD3D11SwapChainUpdatedFunc pSwapChainUpdatedFunc;
	DXLibD3D11ReleaseFunc pReleaseFunc;
}typedef DXLibCallbackFunc;

DXLibCallbackFunc cbFunc = {0};

SET_ACCESSOR(DXLibD3D11RenderFunc, cbFunc.pRenderFunc, D3D11RenderFunc);
SET_ACCESSOR(DXLibD3D11PreRenderFunc, cbFunc.pPreRenderFunc, D3D11PreRenderFunc);
SET_ACCESSOR(DXLibD3D11CreateDeviceFunc, cbFunc.pCreateDeviceFunc, D3D11CreateDeviceFunc);
SET_ACCESSOR(DXLibD3D11SwapChainUpdatedFunc, cbFunc.pSwapChainUpdatedFunc, D3D11SwapChainUpdatedFunc);
SET_ACCESSOR(DXLibD3D11ReleaseFunc, cbFunc.pReleaseFunc, D3D11ReleaseFunc);

HWND hWnd = NULL;

bool DXLibWindowCreated = false;	GET_SET_ACCESSOR(bool, DXLibWindowCreated, WindowCreated);
bool DXLibDeviceCreated = false;	GET_SET_ACCESSOR(bool, DXLibDeviceCreated, DeviceCreated);

// ウィンドウが閉じるとプロセス終了
LRESULT CALLBACK WinProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, iMsg, wParam, lParam);
}

bool DXLibCreateWindow(LPWSTR strWindowName, LPWSTR strClassName)
{
	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(wc));
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WinProc;
	wc.cbClsExtra = wc.cbWndExtra = 0;
	wc.hInstance = NULL;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = strClassName;

	if (!RegisterClass(&wc)) return false;

	RECT rc;
	memset(&rc, 0, sizeof(rc));
	SetRect(&rc, 0, 0, defWindowWidth, defWindowHeight);
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, false);

	hWnd = CreateWindow(strClassName, strWindowName, WS_OVERLAPPEDWINDOW, 0, 0, (rc.right - rc.left), (rc.bottom - rc.top),
		NULL, NULL, NULL, NULL);

	if (hWnd == NULL) return false;

#ifdef _DEBUG
	ZeroMemory(&wc, sizeof(wc));
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = DefWindowProc;
	wc.cbClsExtra = wc.cbWndExtra = 0;
	wc.hInstance = NULL;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = strClassName;

	if (!RegisterClass(&wc)) return false;

	memset(&rc, 0, sizeof(rc));
	SetRect(&rc, 0, 0, defWindowWidth, defWindowHeight);
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, false);

	hWnd = CreateWindow(L"DebugWindow", L"DebugWindow", WS_OVERLAPPEDWINDOW, 0, 0, (rc.right - rc.left), (rc.bottom - rc.top),
		NULL, NULL, NULL, NULL);

	if (hWnd == NULL) return false;

#endif

	// 状態をウィンドウ作成済みに変更
	DXLibSetWindowCreated(true);
	return true;
}

HRESULT DXLibLoadCompiledShader(wchar_t *strShaderFile, ID3DBlob **ppShaderBuffer)
{
	HRESULT hr = S_OK;

	HANDLE hFile = NULL;
	int fileSize = 0;
	DWORD dwReadSize = 0;

	hFile = CreateFile(strShaderFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	fileSize = GetFileSize(hFile, NULL);

	if (fileSize <= 0) {
		return S_FALSE;
	}

	if (FAILED(hr = D3D10CreateBlob(fileSize, ppShaderBuffer)))
	{
		return hr;
	}

	ReadFile(hFile, (*ppShaderBuffer)->GetBufferPointer(), fileSize, &dwReadSize, NULL);

	return hr;

}

// デフォルト設定の適用
void ApplyDefaultSettings(DXLibDeviceSettings *pSettings)
{
	pSettings->d3d11.DriverType = D3D_DRIVER_TYPE_HARDWARE;
	pSettings->d3d11.AdapterOrdinal = 0;
	pSettings->d3d11.AutoCreateDepthStencil = true;
	pSettings->d3d11.AutoDepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	pSettings->d3d11.CreateFlags = 0;
	pSettings->d3d11.DeviceFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	pSettings->d3d11.Output = 0;
	pSettings->d3d11.PresentFlags = 0;
	pSettings->d3d11.sd.BufferCount = 2;
	pSettings->d3d11.sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pSettings->d3d11.sd.BufferDesc.Height = defWindowHeight;
	pSettings->d3d11.sd.BufferDesc.RefreshRate.Numerator = 60;
	pSettings->d3d11.sd.BufferDesc.RefreshRate.Denominator = 1;
	pSettings->d3d11.sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	pSettings->d3d11.sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	pSettings->d3d11.sd.BufferDesc.Width = defWindowWidth;
	pSettings->d3d11.sd.OutputWindow = hWnd;
	pSettings->d3d11.sd.BufferUsage = 32;
	pSettings->d3d11.sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	pSettings->d3d11.sd.OutputWindow = hWnd;
	pSettings->d3d11.sd.SampleDesc.Count = 1;
	pSettings->d3d11.sd.SampleDesc.Quality = 0;
	pSettings->d3d11.sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	pSettings->d3d11.sd.Windowed = 1;
	pSettings->d3d11.SyncInterval = 0;
}

HRESULT UpdateBackBufferDesc()
{
	ID3D11Texture2D *pBackBuffer = NULL;
	IDXGISwapChain *pSwapChain = devResource.pDXGISwapChain;
	if (pSwapChain == NULL) {
		return E_INVALIDARG;
	}
	HRESULT hr = pSwapChain->GetBuffer(0, __uuidof(*pBackBuffer), (LPVOID*)&pBackBuffer);

	DXGI_SURFACE_DESC *pBBSurfaceDesc = &devResource.BackBufferSurfaceDesc;
	ZeroMemory(pBBSurfaceDesc, sizeof(DXGI_SURFACE_DESC));

	if (SUCCEEDED(hr))
	{
		D3D11_TEXTURE2D_DESC TexDesc;
		pBackBuffer->GetDesc(&TexDesc);
		pBBSurfaceDesc->Width = (UINT)TexDesc.Width;
		pBBSurfaceDesc->Height = (UINT)TexDesc.Height;
		pBBSurfaceDesc->Format = TexDesc.Format;
		pBBSurfaceDesc->SampleDesc = TexDesc.SampleDesc;
		SAFE_RELEASE(pBackBuffer);
	}

	return S_OK;
}

HRESULT DXLibSetupD3D11Views(ID3D11DeviceContext *pImmediateContext)
{
	HRESULT hr = S_OK;

	// Setup the viewport to match the backbuffer
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)devResource.BackBufferSurfaceDesc.Width;
	vp.Height = (FLOAT)devResource.BackBufferSurfaceDesc.Height;
	vp.MinDepth = 0;
	vp.MaxDepth = 1;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	pImmediateContext->RSSetViewports(1, &vp);

	// render target viewとdepth stencil viewを出力マージャに関連付け
	
	ID3D11RenderTargetView* pRTV = devResource.pRenderTargetView;
	ID3D11DepthStencilView* pDSV = devResource.pDepthStencilView;
	pImmediateContext->OMSetRenderTargets(1, &pRTV, pDSV);

	return hr;
}

HRESULT DXLibCreateD3D11Views(ID3D11Device *pD3D11Device, ID3D11DeviceContext *pImmediateContext)
{
	HRESULT hr = S_OK;

	IDXGISwapChain* pSwapChain = devResource.pDXGISwapChain;
	ID3D11DepthStencilView* pDSV = NULL;
	ID3D11RenderTargetView* pRTV = NULL;

	// バックバッファをRenderTarget用に取得
	ID3D11Texture2D* pBackBuffer;
	hr = pSwapChain->GetBuffer(0, __uuidof(*pBackBuffer), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;
	D3D11_TEXTURE2D_DESC backBufferSurfaceDesc;
	pBackBuffer->GetDesc(&backBufferSurfaceDesc);

	// RenderTargetViewを作る
	hr = pD3D11Device->CreateRenderTargetView(pBackBuffer, NULL, &pRTV);
	SAFE_RELEASE(pBackBuffer);
	if (FAILED(hr))
		return hr;
	devResource.pRenderTargetView = pRTV;

	if (devSettings.d3d11.AutoCreateDepthStencil)
	{
		// depth stencil viewの設定
		ID3D11Texture2D* pDepthStencil = NULL;
		D3D11_TEXTURE2D_DESC descDepth;
		descDepth.Width = backBufferSurfaceDesc.Width;
		descDepth.Height = backBufferSurfaceDesc.Height;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = devSettings.d3d11.AutoDepthStencilFormat;
		descDepth.SampleDesc.Count = devSettings.d3d11.sd.SampleDesc.Count;
		descDepth.SampleDesc.Quality = devSettings.d3d11.sd.SampleDesc.Quality;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		hr = pD3D11Device->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
		if (FAILED(hr))
			return hr;
		devResource.pDepthBuffer = pDepthStencil;

		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		descDSV.Format = descDepth.Format;
		descDSV.Flags = 0;
		if (descDepth.SampleDesc.Count > 1)
			descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
		else
			descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;

		// depth stencil view を作る
		hr = pD3D11Device->CreateDepthStencilView(pDepthStencil, &descDSV, &pDSV);
		if (FAILED(hr))
			return hr;

		devResource.pDepthStencilView = pDSV;
	}

	hr = DXLibSetupD3D11Views(pImmediateContext);
	if (FAILED(hr))
		return hr;

	if (cbFunc.pSwapChainUpdatedFunc) {
		cbFunc.pSwapChainUpdatedFunc(&devResource);
	}
	return hr;

}

// D3D11のデバイスを実際に作成する
bool CreateD3D11Environment()
{
	ID3D11Device* pD3D11Device = NULL;
	ID3D11DeviceContext* pImmediateContext = NULL;
	IDXGIFactory1* pDXGIFactory = devResource.pDXGIFactory;
	IDXGIAdapter1* pDXGIAdapter = NULL;
	D3D_FEATURE_LEVEL FeatureLevel = D3D_FEATURE_LEVEL_11_0;

	IDXGISwapChain* pSwapChain = NULL;
	DXLibDeviceSettings* pNewDeviceSettings = &devSettings;

	HRESULT hr = S_OK;
	
	pDXGIFactory->EnumAdapters1(devSettings.d3d11.AdapterOrdinal, &pDXGIAdapter);

	// device contextとdeviceを作る
	hr = D3D11CreateDevice(NULL, devSettings.d3d11.DriverType, NULL,
		devSettings.d3d11.CreateFlags, &(devSettings.d3d11.DeviceFeatureLevel), 1,
		D3D11_SDK_VERSION, &pD3D11Device, &FeatureLevel, &pImmediateContext);

	if (FAILED(hr))
	{
		pDXGIAdapter = NULL;
		return false;
	}

	devResource.pD3D11Device = pD3D11Device;
	devResource.pImmediateContext = pImmediateContext;
	devResource.pDXGIAdapter = pDXGIAdapter;
	
	//IDXGIAdapter *pDXGITempAdapter = NULL;	
	//if (SUCCEEDED(hr))
	//{
	//	IDXGIDevice1 *pDXGIDevice = NULL;
	//	pD3D11Device->QueryInterface<IDXGIDevice1>(&pDXGIDevice);
	//	pDXGIDevice->GetAdapter(&pDXGITempAdapter);
	//	pDXGITempAdapter->QueryInterface(__uuidof(IDXGIAdapter1), (LPVOID *)&pDXGIAdapter);
	//	pDXGIAdapter->GetParent(__uuidof(IDXGIFactory1), (LPVOID*)&pDXGIFactory);
	//	SAFE_RELEASE(pDXGITempAdapter);
	//}
	//else {
	//	return false;
	//}

	D3D11_RASTERIZER_DESC rd = {
		D3D11_FILL_SOLID, // FillMode
		D3D11_CULL_NONE,  // CullMode
		FALSE, // FrontCounterClockwise
		0, // DepthBias
		0.0f, //DepthBiasClamp
		0.0f, //
		TRUE, //DepthClipEnable
		FALSE, // ScissorEnable
		TRUE, // MultisampleEnable
		FALSE, //AntialiasedLineEnable
	};

	ID3D11RasterizerState *pRS = NULL;
	hr = pD3D11Device->CreateRasterizerState(&rd, &pRS);
	if (FAILED(hr)) {
		return false;
	}

	pImmediateContext->RSSetState(pRS);
	SAFE_RELEASE(pRS);

	hr = pDXGIFactory->CreateSwapChain(pD3D11Device, &devSettings.d3d11.sd, &pSwapChain);
	if (FAILED(hr)) {
		return false;
	}

	devResource.pDXGISwapChain = pSwapChain;


	UpdateBackBufferDesc();
	
	if (cbFunc.pCreateDeviceFunc != NULL) {
		cbFunc.pCreateDeviceFunc(&devResource);
	}

	DXLibCreateD3D11Views(pD3D11Device, pImmediateContext);

	return true;
}

// IDXGIFactoryを生成して、D3D11のデバイスを作成する。
bool DXLibChangeDevice(DXLibDeviceSettings *newSettings)
{
	//DXLibDeviceSettings *oldSettings = &devSettings;
	IDXGIFactory1  *pDXGIFactory = devResource.pDXGIFactory;

	if (newSettings == NULL)
		return false;

	if (pDXGIFactory == NULL)
	{
		if (FAILED(CreateDXGIFactory1(__uuidof(IDXGIFactory1), (LPVOID*)&pDXGIFactory)))
		{
			return false;
		}
	}

	devResource.pDXGIFactory = pDXGIFactory;

	// ウィンドウを表示
	if (!IsWindowVisible(hWnd))
		ShowWindow(hWnd, SW_SHOW);

	CreateD3D11Environment();
}

// D3D11デバイスの作成
void DXLibCreateDevice(int width, int height)
{
	DXLibDeviceSettings newSettings;
	memset(&newSettings, 0, sizeof(devSettings));
	ApplyDefaultSettings(&devSettings);

	DXLibChangeDevice(&devSettings);
}

void DXLibRender()
{
	HRESULT hr;

	ID3D11Device* pDevice = devResource.pD3D11Device;
	if (NULL == pDevice)
		return;

	ID3D11DeviceContext* pImmediateContext = devResource.pImmediateContext;
	if (NULL == pImmediateContext)
		return;

	IDXGISwapChain* pSwapChain = devResource.pDXGISwapChain;
	if (NULL == pSwapChain)
		return;

	if (cbFunc.pPreRenderFunc) {
		cbFunc.pPreRenderFunc(&devResource);
	}

	// プリレンダリング後にレンダリングパスを戻す
	ID3D11RenderTargetView* pRTV = devResource.pRenderTargetView;
	ID3D11DepthStencilView* pDSV = devResource.pDepthStencilView;
	pImmediateContext->OMSetRenderTargets(1, &pRTV, pDSV);

	if (cbFunc.pRenderFunc != NULL) {
		cbFunc.pRenderFunc(&devResource);
	}

	pSwapChain->Present(0, 0);
}

void DXLibMainLoop()
{
	MSG msg;

	PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			DXLibRender();
		}
	}
}

void DXLibRelease()
{
	cbFunc.pReleaseFunc(&devResource);

	SAFE_RELEASE(devResource.pDepthStencilView);
	SAFE_RELEASE(devResource.pRenderTargetView);
	SAFE_RELEASE(devResource.pDepthBuffer);
	SAFE_RELEASE(devResource.pImmediateContext);
	SAFE_RELEASE(devResource.pD3D11Device);
	SAFE_RELEASE(devResource.pDXGISwapChain);
	SAFE_RELEASE(devResource.pDXGIAdapter);
	SAFE_RELEASE(devResource.pDXGIFactory);
}


DXLibCompiledShader::DXLibCompiledShader()
{
	m_dwSize = 0;
	m_pBuffer = NULL;
}

DXLibCompiledShader::~DXLibCompiledShader()
{

}

void DXLibCompiledShader::Release()
{
	free(m_pBuffer);
	m_dwSize = 0;
}

void *DXLibCompiledShader::GetBufferPointer()
{
	return m_pBuffer;
}

DWORD DXLibCompiledShader::GetBufferSize()
{
	return m_dwSize;
}
