struct PSInput
{
	float4 Position : SV_POSITION;
	float4 Normal : NORMAL0;
};

float4 main(PSInput input) : SV_TARGET
{
	return input.Normal;
}