struct PSInput
{
	float4 Position : SV_POSITION;
	float4 Normal : NORMAL0;
	float4 Albedo : COLOR0;
	float4 Specular : COLOR1;
};

float4 main(PSInput input) : SV_TARGET
{
	return input.Albedo;
}