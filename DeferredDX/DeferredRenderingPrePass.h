#pragma once
#include <d3d11.h>

// 開放必須
struct _RenderingPassResource
{
	
}
typedef RenderingPassResource;

// 開放不要
struct _RenderingPassSettings
{
	//UINT numRenderTarget;

	//ID3D11Texture2D	**ppTargetBuffers;
	//ID3D11Texture2D *pDepthBuffer;

	ID3D11VertexShader	*pVertexShader;
	ID3D11PixelShader	*pPixelShader;
}
typedef RenderingPassSettings;

class RenderingPass
{
private:
	UINT numRenderTarget;
	RenderingPassSettings passSettings;

	// 開放が必要
	ID3D11RenderTargetView	**ppRenderTargetViews;	
	ID3D11DepthStencilView	*pDepthStencilView;		


public:
	RenderingPass();
	~RenderingPass();

	// 法線・鏡面反射成分のバッファ
	HRESULT UpdateViews(ID3D11Device *pDevice, UINT numRenderTarget, ID3D11Texture2D **ppRenderTargetBuffers, ID3D11Texture2D *pDepthBuffers);
	void SetD3D11Shaders(ID3D11VertexShader *pVertexShader, ID3D11PixelShader *pPixelShader);
	void ReleaseViews();

	void RenderPass(ID3D11DeviceContext *pImmediateContext);
};

HRESULT DXLib_CreateDeferredBasePassResource(ID3D11Device *pDevice);
void DXLib_RenderDeferredBasePass(ID3D11DeviceContext *pImmediateContext);
void DXLib_ReleaseDeferredBasePassResource();
