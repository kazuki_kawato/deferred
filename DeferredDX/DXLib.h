#pragma once
/*
DirectX関係を集約
*/
#include <d3d11.h>
#include <d3dxmath.h>
#include <vector>

struct DXLibDeviceSetting11
{
	UINT AdapterOrdinal;
	D3D_DRIVER_TYPE DriverType;
	UINT Output;
	DXGI_SWAP_CHAIN_DESC sd;
	UINT32 CreateFlags;
	UINT32 SyncInterval;
	DWORD PresentFlags;
	bool AutoCreateDepthStencil;
	DXGI_FORMAT AutoDepthStencilFormat;
	D3D_FEATURE_LEVEL DeviceFeatureLevel;
};

struct _DXLibDeviceResource
{
	ID3D11Device		*pD3D11Device;
	ID3D11DeviceContext	*pImmediateContext;

	IDXGIFactory1		*pDXGIFactory;
	IDXGIAdapter1		*pDXGIAdapter;
	IDXGISwapChain		*pDXGISwapChain;

	DXGI_SURFACE_DESC	BackBufferSurfaceDesc;

	ID3D11RenderTargetView	*pRenderTargetView;
	ID3D11DepthStencilView	*pDepthStencilView;
	ID3D11Texture2D			*pDepthBuffer;

}
typedef DXLibDeviceResource;

enum DXLIB_DEVICE_VERSION {
	DX_DEVICE_D3D9,
	DX_DEVICE_D3D11,
};

struct _DXLibDeviceSettings
{
	DXLIB_DEVICE_VERSION	ver;
	DXLibDeviceSetting11	d3d11;
}
typedef DXLibDeviceSettings;

class DXLibCompiledShader
{
private:
	void *m_pBuffer;
	DWORD m_dwSize;

public:
	DXLibCompiledShader();
	~DXLibCompiledShader();

	void Release();
	void *GetBufferPointer();
	DWORD GetBufferSize();
};

// コールバック関数
typedef void (*DXLibD3D11RenderFunc)(DXLibDeviceResource *pResource);
typedef void (*DXLibD3D11PreRenderFunc)(DXLibDeviceResource *pResource);
typedef HRESULT(*DXLibD3D11CreateDeviceFunc)(DXLibDeviceResource *pResource);
typedef HRESULT(*DXLibD3D11SwapChainUpdatedFunc)(DXLibDeviceResource *pResource);
typedef void (*DXLibD3D11ReleaseFunc)(DXLibDeviceResource *pResource);

// セッター用API (定義はマクロで)
void DXLibSetD3D11RenderFunc(DXLibD3D11RenderFunc tFunc); // Presentを呼ぶ直前で呼ばれる
void DXLibSetD3D11PreRenderFunc(DXLibD3D11PreRenderFunc tFunc); // マルチパスレンダリングのときに使う
void DXLibSetD3D11CreateDeviceFunc(DXLibD3D11CreateDeviceFunc tFunc); // Device作成後に呼び出される
void DXLibSetD3D11SwapChainUpdatedFunc(DXLibD3D11SwapChainUpdatedFunc tFunc); //SwapChain作成時(更新時)に呼び出す
void DXLibSetD3D11ReleaseFunc(DXLibD3D11ReleaseFunc tFunc); //デバイス等を開放する前に呼び出す



bool DXLibCreateWindow(LPWSTR strWindowName, LPWSTR strClassName);
HRESULT DXLibLoadCompiledShader(wchar_t *strShaderFile, ID3DBlob **ppShaderBuffer);
void DXLibCreateDevice(int width, int height);
void DXLibMainLoop();
void DXLibRelease();